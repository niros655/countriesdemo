import React, { Component } from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity, WebView} from 'react-native'
import SVGImage from 'react-native-svg-image';
class CountryListItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {item, showFlag, handlePress} = this.props;
        const {flag, name, nativeName, borders} = item;
        return <TouchableOpacity style={styles.container} onPress={()=>handlePress ? handlePress(borders) : null}>
            <View style={styles.imageContainer}>
                {showFlag && <SVGImage style={styles.image} source={{uri: flag}}/>}
            </View>
            <View>
                <Text style={styles.title}>{name}</Text>
                <Text style={styles.subTitle}>{nativeName}</Text>
            </View>
        </TouchableOpacity>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },

    title: {
        fontWeight: 'bold',
        textAlign: 'left',
    },

    subTitle: {
        fontSize: 12,
        color: '#BBB',
        textAlign: 'left',
    },

    imageContainer: {
        width: 45,
        height: 45,
        margin: 12,
        backgroundColor: '#FFF',

    },

    image: {
        width: 45,
        height: 45,
    }
});

export default CountryListItem;