import React, { Component } from 'react'
import {View, StyleSheet, PixelRatio} from 'react-native';

class CountryBorders extends Component{
    constructor(props) {
        super(props);
    }

    render(){
        return <View style={styles.separator}/>
    }
}

const styles = StyleSheet.create({
    separator: {
        backgroundColor: '#CCC',
        height: 1 / PixelRatio.get(),
        marginLeft: 15,
    }
});

export default CountryBorders;