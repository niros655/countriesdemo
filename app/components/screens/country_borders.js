import React, { Component } from 'react'
import {StyleSheet, FlatList, Text} from 'react-native';
import CountryListItem from '../views/country_list_item'
import Separator from '../views/separator'

class CountryBorders extends Component{
    constructor(props) {
        super(props);
    }

    static navigationOptions = {
        title: 'Country Borders',
    };

    render(){
        const {bordersCountries} = this.props.navigation.state.params;
        return <FlatList
            style={styles.container}
            data={Object.values(bordersCountries)}
            keyExtractor={(item, index)=> index}
            renderItem={({item}) => <CountryListItem item={item} showFlag={true}/>}
            removeClippedSubviews={false}
            enableEmptySections={true}
            ListEmptyComponent={()=> <Text>No countries to show...</Text>}
            ItemSeparatorComponent={()=> <Separator/>}
        />
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});

export default CountryBorders;