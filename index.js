import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';

import App from './App';
import CountryBorders from './app/components/screens/country_borders'

const MainNavigation = StackNavigator({
    App: { screen: App },
    CountryBorders: { screen: CountryBorders },
});

AppRegistry.registerComponent('CountriesDemo', () => MainNavigation);
