import React, { Component } from 'react'
import {StyleSheet, Text, View, FlatList, AsyncStorage, ActivityIndicator, RefreshControl} from 'react-native'
import CountryListItem from './app/components/views/country_list_item'
import Separator from './app/components/views/separator'
const COUNTRIES_DATA_KEY = "@CountriesData";
const API_HOST = "https://restcountries.eu/rest/v2";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countries: {},
            refreshing: false,
        };
    }

    static navigationOptions = {
        title: 'Countries',
    };


    async getAllCountriesData() {
        try {
            // Get countries data
            let response = await fetch(`${API_HOST}/all`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            });

            let responseJson = await response.json();

            // Initialize object of countries
            const countriesObj = {};

            if(Array.isArray(responseJson)) {
                responseJson.map(country => {
                    // Get relevant data
                    const {alpha3Code, name, nativeName, flag, borders} = country;
                    countriesObj[alpha3Code] = {name, nativeName, flag, borders};
                });

                // Save cache of countries data locally
                AsyncStorage.setItem(COUNTRIES_DATA_KEY, JSON.stringify(countriesObj));

                this.setState({countries: countriesObj})
            }
            else {
                alert('Something went wrong...');
            }

        } catch(error) {
            alert(error);
        }
    }

    onHandlePress(borders) {
        const { navigate } = this.props.navigation;
        const {countries} = this.state;

        // Get borders countries
        let bordersCountries = [];
        borders.map(alpha3Code => bordersCountries.push(countries[alpha3Code]));

        // Navigate to 'Country borders' screen
        navigate('CountryBorders', {bordersCountries})
    }

    onRefresh() {
        this.setState({refreshing: true});
        this.getAllCountriesData().then(() => this.setState({refreshing: false}))
    }

    componentWillMount() {
      // Check if countries exist locally
        AsyncStorage.getItem(COUNTRIES_DATA_KEY, (err, result) => {
          // Error occur
          if(err)
            alert(err);

          else {
            const countries = JSON.parse(result);
            if(countries) // Data exist
              this.setState({countries});
            else // Get data from server
                this.getAllCountriesData().done();
          }
        })
    }

    render() {
      const {countries} = this.state;
      return <FlatList
          style={styles.container}
          data={Object.values(countries)}
          keyExtractor={(item, index)=> index}
          renderItem={({item}) => <CountryListItem item={item} showFlag={false} handlePress={this.onHandlePress.bind(this)}/>}
          removeClippedSubviews={false}
          enableEmptySections={true}
          ListEmptyComponent={()=> <ActivityIndicator/>}
          ItemSeparatorComponent={()=> <Separator/>}
          refreshControl={
              <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.onRefresh.bind(this)}
              />
          }
        />
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});